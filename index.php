<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    $sheep = new Animal("shaun");
    echo "Name = " .$sheep->name ."<br>";
    echo "Legs = " .$sheep->legs ."<br>";
    echo "Cold blooded = " .$sheep->cold_blooded ."<br><br>";

    $Buduk = new Frog("Buduk");
    echo "Name = " .$Buduk->name ."<br>";
    echo "Legs = " .$Buduk->legs ."<br>";
    echo "Cold blooded = " .$Buduk->cold_blooded ."<br>";
    echo $Buduk->Jump() . "<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name = " .$sungokong->name ."<br>";
    echo "Legs = " .$sungokong->legs ."<br>";
    echo "Cold blooded = " .$sungokong->cold_blooded ."<br>";
    echo $sungokong->Yell() . "<br><br>";

    
?>